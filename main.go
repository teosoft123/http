package main

import (
	"bitbucket.com/teosoft123/http/httpserver"
)

func main() {
	server := httpserver.HttpServer{8080, "/msg"}
	server.Run()
}
