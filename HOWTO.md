# HOWTO #

Create a new or use existing Go development tree. Say you have root of this tree in TREE: export TREE=<wherever you have it>
Now, create directory like this:

    mkdir -p $TREE/src/bitbucket.com/teosoft123

You need to git pull from https://teosoft123@bitbucket.org/teosoft123/http.git

    cd $TREE/src/bitbucket.com/teosoft123
    git pull https://teosoft123@bitbucket.org/teosoft123/http.git
    
Run it:

    see log below

### Log for running everything ###

```
oleg@Olegs-iMac:~/projects/go-workspace/src/bitbucket.com/teosoft123/http$ tree
.
├── README.md
├── http.iml
├── httpserver
│   ├── HttpServer.go
│   └── Types.go
├── main.go
└── test
    └── HttpServer_test.go

2 directories, 6 files
oleg@Olegs-iMac:~/projects/go-workspace/src/bitbucket.com/teosoft123/http$ go run main.go &
[1] 17387
oleg@Olegs-iMac:~/projects/go-workspace/src/bitbucket.com/teosoft123/http$ go test -v test/HttpServer_test.go
=== RUN   TestHttpServerUp
--- PASS: TestHttpServerUp (0.00s)
	HttpServer_test.go:16: attempting to connect... server must be running
=== RUN   TestHttpServerGet
--- PASS: TestHttpServerGet (0.00s)
	HttpServer_test.go:30: trying to issue GET request, to make sure server doesn't allow it
=== RUN   TestHttpServerPost
--- PASS: TestHttpServerPost (0.00s)
	HttpServer_test.go:46: reading server response, making sure it is as expected
PASS
ok  	command-line-arguments	0.011s
oleg@Olegs-iMac:~/projects/go-workspace/src/bitbucket.com/teosoft123/http$ fg
go run main.go
^Csignal: interrupt
oleg@Olegs-iMac:~/projects/go-workspace/src/bitbucket.com/teosoft123/http$
```