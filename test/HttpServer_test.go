package test

import (
	"testing"
	"net/http"
	"time"
	"encoding/json"
	"bitbucket.com/teosoft123/http/httpserver"
	"os"
)

// NOTE: For these tests to pass, HttpServer must be running.

func runTests(m *testing.M, c chan int) {
	c <- m.Run()
}

func TestMain(m *testing.M) {
	c := make(chan int, 1)

	server := httpserver.HttpServer{8080, "/msg"}
	go server.Run()
	go runTests(m, c)
	os.Exit(<-c)
}

var testHttpClient = &http.Client{Timeout: 10 * time.Second}

func TestHttpServerUp(t *testing.T) {
	t.Log("attempting to connect... server must be running")
	response, e := testHttpClient.Get("http://localhost:8080")
	defer response.Body.Close()
	if e != nil {
		t.Errorf("Http server is not running")
	}
}

func TestHttpServerGet(t *testing.T) {
	response, e := testHttpClient.Get("http://localhost:8080")
	defer response.Body.Close()
	if e != nil {
		t.Errorf("Http server is not running")
	}
	t.Log("trying to issue GET request, to make sure server doesn't allow it")
	if response.StatusCode != 404 {
		// GET not supported on that URL
		t.Errorf("Status code: Expected <%d>, Actual <%d>", 404, response.StatusCode)
	}
}

func TestHttpServerPost(t *testing.T) {
	response, e := testHttpClient.Post("http://localhost:8080/msg", "application/json", nil)
	defer response.Body.Close()
	if e != nil {
		t.Errorf("Http server is not running")
	}
	if sc := response.StatusCode; sc != 200 {
		t.Errorf("Status code: Expected <%d>, Actual <%d>", 200, response.StatusCode)
	}
	t.Log("reading server response, making sure it is as expected")
	myData := httpserver.Message{}
	json.NewDecoder(response.Body).Decode(&myData)

	if expected := "Hello world"; myData.Message != expected {
		t.Errorf("Message: Expected <%s>, Actual <%s>", expected, myData.Message)
	}

}
