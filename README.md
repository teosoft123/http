# README #

See hello project for environment setup

### Simple Http Restful server with tests ###

After you manage to compile and run it, play with curl:

this should work:

   curl -XPOST http://localhost:8080/msg

this should fail:

   curl http://localhost:8080/msg

this will give you more data that you can check in Go tests:

   curl -v -XPOST http://localhost:8080/msg

### Used the following resources to crank out this cruft ###

   https://thenewstack.io/make-a-restful-json-api-go/

   https://github.com/gorilla/mux#examples

### How to run ###

In the directory with *.go sources do:

1) run server - note '&' will put server in background:

go run go run Types.go HttpServer.go &

2) run tests:

go test -v Types.go HttpServer_test.go

3) bring server to foreground and kill it:

fg
^C
