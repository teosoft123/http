package httpserver

import (
	"log"
	"net/http"
	"encoding/json"
	"github.com/gorilla/mux"
	"fmt"
)

type HttpServer struct {
	Port    int
	BaseUrl string
}

func (s *HttpServer) MessageHandler(w http.ResponseWriter, _ *http.Request) {
	m := Message{Message: "Hello world"}
	if err := json.NewEncoder(w).Encode(m); err != nil {
		panic(err)
	}
}

func (s *HttpServer) Run() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc(s.BaseUrl, s.MessageHandler).Methods("POST")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", s.Port), router))
}
